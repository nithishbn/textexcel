package textExcel;

//Update this file with your own code.

public class SpreadsheetLocation implements Location {
    private int row;
    private int col;

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public SpreadsheetLocation(String cellName) {
        cellName = cellName.trim();
        // treats char like an integer and converts letter to column number
        this.col = Character.toUpperCase(cellName.charAt(0)) - 'A';
        this.row = Integer.parseInt(cellName.substring(1)) - 1;
    }

}
