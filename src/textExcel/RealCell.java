package textExcel;

public abstract class RealCell implements Cell {
    private String value;
    private boolean isUse = false; // very important to prevent circular reference in formulaCell! defaulted to false in all other cells that don't require evaluation
    public RealCell(String value) {
        this.value = value;
    }

    public boolean isUse() {
        return isUse;
    }

    public void setUse(boolean use) {
        isUse = use;
    }

    public String abbreviatedCellText() {
        // gets double value and truncates past 10 characters
        return (getDoubleValue() + "          ").substring(0, 10);
    }

    public String fullCellText() {
        return value;
    }

    // to be implemented in all RealCell child classes
    public abstract double getDoubleValue();


}
