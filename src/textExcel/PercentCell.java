package textExcel;

public class PercentCell extends RealCell {

    public PercentCell(String value) {
        // input value converted to decimal and stored in value field
        super((Double.parseDouble(value.substring(0,value.length()-1))/100)+"");
    }

    public double getDoubleValue() {
        return (Double.parseDouble(fullCellText()));
    }

    // overridden because value is stored as decimal so it needs to be converted back into a regular and rounded percentage
    public String abbreviatedCellText() {
        // casting to int truncates extra decimal stuff for viewing
        String newValue = (int)(getDoubleValue()*100) + "%          ";
        return newValue.substring(0,10);
    }

    public String getCellType() {
        return "PercentCell";
    }
}
