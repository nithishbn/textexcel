package textExcel;

import java.util.List;

// made a separate Calculator class so that it would be easier to manage code instead of making calculation specific to FormulaCell
public class Calculator {
    private Grid spreadsheet; // keeps track of spreadsheet

    public Calculator(Grid spreadsheet) {
        this.spreadsheet = spreadsheet;
    }

    // takes an ArrayList of tokens passed in from the FormulaCell and evaluates it
    public String evaluate(List<String> tokens) {
        assert (tokens != null);
        if (tokens.size() > 1) {
            // assuming calling a function on cells example: AVG, SUM
            if (tokens.size() == 2) {
                switch (tokens.get(0).toLowerCase()) {
                    case "sum":
                        return "" + sum(tokens.get(1));
                    case "avg":
                        return "" + average(tokens.get(1));
                    default:
                        return "#ERROR";
                }
            }
            // if there are only 3 tokens given or the precedence of the second operator is greater than the precedence of the first operator, do this
            else if (tokens.size() == 3 || precedence(tokens.get(1)) >= precedence(tokens.get(3))) {
                String res = calculate(tokens.get(0), tokens.get(1), tokens.get(2));
                if (res.startsWith("#ERROR")) {
                    return res;
                }
                tokens.set(0, res);
                tokens.remove(1);
                tokens.remove(1);
            }
            // if the precedence of the second operator is larger, evaluate that first
            else {
                String res = calculate(tokens.get(2), tokens.get(3), tokens.get(4));
                if (res.startsWith("#ERROR")) {
                    return res;
                }
                tokens.set(2, res);
                tokens.remove(3);
                tokens.remove(3);
            }
            // recursion because ArrayLists change size so only the first 3 tokens or the next 3 tokens can be taken to evaluate
            evaluate(tokens);
        }
        // returns the final value since there should only be 1 left
//        assert (tokens.size() == 1);
        System.out.println(tokens);
        return tokens.get(0);
    }

    // gets the average value of a certain range of cells
    private String average(String formula) {
        // adds all the values up in that range first
        String sum = sum(formula);
        if (sum.equals("#ERROR")) {
            return sum;
        }
        int index = formula.indexOf("-");
        Location l1 = new SpreadsheetLocation(formula.substring(0, index));
        Location l2 = new SpreadsheetLocation(formula.substring(index + 1));
        // gets how many rows there are
        int rowWidth = l2.getRow() - l1.getRow() + 1;
        // gets how many columns there are
        int colWidth = l2.getCol() - l1.getCol() + 1;
        // returns average based on number of rows and columns
        assert (rowWidth > 0 && colWidth > 0);
        return "" + (Double.parseDouble(sum) / (rowWidth * colWidth));
    }

    // gets the sum of a range of cells
    private String sum(String formula) {
        int index = formula.indexOf("-");
        assert (index != -1);
        Location l1 = new SpreadsheetLocation(formula.substring(0, index)); // location 1
        Location l2 = new SpreadsheetLocation(formula.substring(index + 1)); // location 2
        double result = 0;
        // goes row by row, then column by column to get cells from location 1 to location 2
        for (int row = l1.getRow(); row <= l2.getRow(); row++) {
            for (int col = l1.getCol(); col <= l2.getCol(); col++) {
                // the location of the next cell
                String location = ((char) (col + 'A')) + (row + 1 + "");
                Cell cell = spreadsheet.getCell(new SpreadsheetLocation(location));
                if (cell.getCellType().equals("TextCell")) {
                    return "#ERROR";
                }
                if (!cell.getCellType().equals("EmptyCell")) {
                    // gets value of cell and adds it to the result
                    result += ((RealCell) cell).getDoubleValue();
                }
                if (((RealCell) spreadsheet.getCell(new SpreadsheetLocation(location))).isUse()) {
                    return "#ERROR";
                }

            }
        }
        // returns value
        return "" + result;
    }

    // returns 1 or 0 depending on the precedence of the operator
    public int precedence(String operator) {
        if (operator.equals("*") || operator.equals("/")) {
            return 1;
        }
        return 0;
    }

    // takes two operands and an operator and performs the operation of the operator
    public String calculate(String operand1, String operator1, String operand2) {
        // if the first operand refers to a cell
        if (operand1.substring(0, 1).matches("[a-zA-Z]")) {
            Location location = new SpreadsheetLocation(operand1);
            assert (!spreadsheet.getCell(location).getCellType().equals("TextCell"));
            if (spreadsheet.getCell(location).getCellType().equals("TextCell")) {
                return "#ERROR";
            }
            if (((RealCell) spreadsheet.getCell(location)).isUse()) {
                return "#ERROR";
            }
            operand1 = "" + ((RealCell) spreadsheet.getCell(location)).getDoubleValue();
        }
        // if the second operand refers to a cell
        if (operand2.substring(0, 1).matches("[a-zA-Z]")) {
            SpreadsheetLocation location = new SpreadsheetLocation(operand2);
            assert (!spreadsheet.getCell(location).getCellType().equals("TextCell"));
            if (spreadsheet.getCell(location).getCellType().equals("TextCell")) {
                return "#ERROR";
            }
            if (((RealCell) spreadsheet.getCell(location)).isUse()) {
                return "#ERROR";
            }
            operand2 = "" + ((RealCell) spreadsheet.getCell(location)).getDoubleValue();
        }
        double op1 = Double.parseDouble(operand1);
        double op2 = Double.parseDouble(operand2);
        switch (operator1) {
            case "*":
                return op1 * op2 + "";
            case "/":
                return op1 / op2 + "";
            case "+":
                return op1 + op2 + "";
            default:
                return op1 - op2 + "";
        }
    }


}