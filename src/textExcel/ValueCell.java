package textExcel;

public class ValueCell extends RealCell {
    public ValueCell(String value) {
        super(value);
    }

    public String getCellType() {
        return "ValueCell";
    }

    public double getDoubleValue() {
        return Double.parseDouble(fullCellText());
    }
}
