package textExcel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import java.util.ArrayList;
import java.util.Scanner;

public class Spreadsheet implements Grid {
    // creates a spreadsheet filled with Cells
    private Cell[][] spreadsheet = new Cell[getRows()][getCols()];
    private ArrayList<String> commandHistory = new ArrayList<>();
    private boolean recording;
    private int commandLimit;
    private String fileName;

    // default constructor that constructs the grid using the reset method
    public Spreadsheet() {
        reset();
    }

    // builds the grid
    private void reset() {
        for (int i = 0; i < getRows(); i++) {
            for (int j = 0; j < getCols(); j++) {
                spreadsheet[i][j] = new EmptyCell();
            }
        }
    }

    // processes commands passed in from the main method
    public String processCommand(String command) {
        // easy to add error handling stuff here hopefully
        String[] tokens = command.split(" ");
        if (command.equals("")) {
            return "";
        }
        else if (command.toLowerCase().equals("help")){
            System.out.println("Help!");
            System.out.println("There are 4 types of Cell");
        }
        else if (tokens.length == 1) {
            // cell inspection
            SpreadsheetLocation loc = new SpreadsheetLocation(tokens[0]);
            if (loc.getRow() >= getRows() || loc.getCol() >= getCols()) return "ERROR: Invalid command.";
            return getCell(loc).fullCellText();
        }
        // clear command without cell specified
        else if (command.toLowerCase().equals("clear")) {
            spreadsheet = new Cell[getRows()][getCols()];
            reset();

        }
        // clear command with cell specified
        else if (command.toLowerCase().startsWith("clear") && tokens.length == 2) {
            SpreadsheetLocation loc = new SpreadsheetLocation(tokens[1]);
            spreadsheet[loc.getRow()][loc.getCol()] = new EmptyCell();

        } else if (command.startsWith("history")) {
            if (command.contains("start")) {
                commandLimit = Integer.parseInt(tokens[2]);
                recording = true;
            } else if (command.contains("stop")) {
                for (int i = commandHistory.size() - 1; i >= 0; i--) {
                    commandHistory.remove(i);
                }
                recording = false;
            } else if (command.contains("display")) {
                for (String comm : commandHistory) {
                    System.out.println(comm);
                }
            } else if (command.contains("clear")) {
                int num = Integer.parseInt(tokens[2]);
                for (int i = 0; i < num; i++) {
                    commandHistory.remove(0);
                }
            }
            return "";
        }
        // saving spreadsheet to file
        else if (command.toLowerCase().startsWith("save")) {
            if (tokens.length > 1) {
                saveFile(tokens[1]);
                fileName = tokens[1];
                return getGridText();
            }
            // saves to previously saved file
            if (fileName == null) {
                System.out.println("Save spreadsheet to a file first!");
                return getGridText();
            }
            saveFile(fileName);
        }
        // opening file and loading spreadsheet
        else if (command.toLowerCase().startsWith("open")) {
            openFile(tokens[1]);
        }
        // text cell
        else if (command.contains("=") && command.contains("\"") && tokens.length > 2) {
            SpreadsheetLocation loc = new SpreadsheetLocation(tokens[0]);
            if (loc.getRow() >= getRows() || loc.getCol() >= getCols()) return "ERROR: Invalid command.";
            spreadsheet[loc.getRow()][loc.getCol()] = new TextCell(concatenate(tokens));
        }
        // checks if it's a value cell
        else if (command.contains("=") && !command.contains("%") && (!command.contains("(") || !command.contains(")")) && tokens.length > 2) {
            SpreadsheetLocation loc = new SpreadsheetLocation(tokens[0]);
            if (loc.getRow() >= getRows() || loc.getCol() >= getCols()) return "ERROR: Invalid command.";
            spreadsheet[loc.getRow()][loc.getCol()] = new ValueCell(concatenate(tokens));

        }
        // check if it's a percent
        else if (command.contains("=") && !command.contains("(") && tokens.length > 2) {
            SpreadsheetLocation loc = new SpreadsheetLocation(tokens[0]);
            if (loc.getRow() >= getRows() || loc.getCol() >= getCols()) return "ERROR: Invalid command.";
            spreadsheet[loc.getRow()][loc.getCol()] = new PercentCell(concatenate(tokens));

        }
        // checks if it is a formula
        else if (command.contains("=") && tokens.length > 2 && command.contains("(")) {
            SpreadsheetLocation loc = new SpreadsheetLocation(tokens[0]);
            if (loc.getRow() >= getRows() || loc.getCol() >= getCols()) return "ERROR: Invalid command.";
            spreadsheet[loc.getRow()][loc.getCol()] = new FormulaCell(concatenate(tokens), this);

        }  else {
            return "ERROR: Invalid command.";
        }
        return getGridText();
    }

    // saves existing spreadsheet to a file
    private void saveFile(String fileName) {
        File file = new File(fileName);
        try {
            PrintWriter f = new PrintWriter(file);
            assert (spreadsheet != null);
            for (int row = 0; row < spreadsheet.length; row++) {
                for (int col = 0; col < spreadsheet[0].length; col++) {
                    Cell cell = spreadsheet[row][col];
                    if (cell.getCellType().equals("EmptyCell")) {
                        continue;
                    }
                    f.print((char) (col + 'A'));
                    f.print(row + 1 + ",");
                    f.print(cell.getCellType() + ",");
                    if (cell.getCellType().equals("PercentCell")) {
                        // converts percent to a double and adds it to the file
                        f.print(Double.parseDouble(cell.fullCellText()));
                    } else {
                        f.print(cell.fullCellText());
                    }
                    f.println();
                }
            }
            f.close();
        } catch (FileNotFoundException e) {
            System.out.println("ERROR: Invalid command.");
        }
    }

    // opens a file that has been saved
    private void openFile(String fileName) {
        File file = new File(fileName);
        try {
            Scanner console = new Scanner(file);
            reset();
            while (true) {
                if (!console.hasNext()) {
                    break;
                }
                String[] tokens = console.nextLine().split(",", 3);
                int col = tokens[0].charAt(0) - 'A';
                int row = Integer.parseInt(tokens[0].substring(1)) - 1;
                // added switch block because it's easier for this case
                switch (tokens[1]) {
                    case "TextCell":
                        spreadsheet[row][col] = new TextCell(tokens[2]);
                        break;
                    case "ValueCell":
                        spreadsheet[row][col] = new ValueCell(tokens[2]);
                        break;
                    case "PercentCell":
                        // value is converted to an actual percent from a double so that PercentCell can handle it properly
                        spreadsheet[row][col] = new PercentCell(Double.parseDouble(tokens[2]) * 100 + "%");
                        break;
                    case "FormulaCell":
                        spreadsheet[row][col] = new FormulaCell(tokens[2], this);
                        break;
                }
            }
            console.close();
        } catch (FileNotFoundException e) {
            System.out.println("ERROR: Invalid command.");
        }

    }

    // essentially undos the stuff the split function does but does it so that the string value can have multiple spaces in it
    // works with formula cell formulae as well!
    private String concatenate(String[] tokens) {
        int length = tokens.length - 2;
        String[] newTokens = new String[length];
        int index = 0;
        for (int i = 2; i < tokens.length; i++) {
            newTokens[index] = tokens[i];
            index++;
        }
        return String.join(" ", newTokens);
    }


    // can be changed to affect the entire program!
    public int getRows() {
        return 20;
    }

    // can be changed to affect the entire program!
    public int getCols() {
        return 12;
    }

    // returns the cell using the row and column from location
    public Cell getCell(Location loc) {
        return spreadsheet[loc.getRow()][loc.getCol()];
    }

    // prints out the spreadsheet
    public String getGridText() {
        String gridText = "   ";
        int rowNum = 1;
        // prints the column headers
        for (int i = 0; i < spreadsheet[0].length; i++) {
            gridText += "|" + (char) (i + 'A') + "         ";
        }
        gridText += "|\n";
        // prints out the cells
        for (Cell row[] : spreadsheet) {
            gridText += String.format("%-3d", rowNum);
            rowNum++;
            for (Cell col : row) {
                gridText += "|" + col.abbreviatedCellText();
            }
            gridText += "|\n";
        }
        return gridText;
    }

    public void recordCommand(String command) {
        if (recording && !command.startsWith("history")) {
            if (commandHistory.size() + 1 > commandLimit) {
                commandHistory.remove(0);
            }
            commandHistory.add(command);
        }
    }
}

