package textExcel;

public class TextCell implements Cell {
    private String value;

    public TextCell(String str) {
        setValue(str);
    }

    public String abbreviatedCellText() {
        if (value.length() == 0) {
            return "";
        }
        String value = this.value.substring(1, this.value.length() - 1) + "          ";
        return value.substring(0, 10);
    }

    public String fullCellText() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCellType() {
        return "TextCell";
    }
}
