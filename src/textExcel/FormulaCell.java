package textExcel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FormulaCell extends RealCell {

    private Calculator calculator;
    private boolean isError = false;

    public FormulaCell(String value, Grid spreadsheet) {
        super(value);
        // creates a new calculator object that takes in a copy of the
        // spreadsheet upon execution
        this.calculator = new Calculator(spreadsheet);
    }

    public double getDoubleValue() {
        // removes parentheses
        String formula = fullCellText().substring(2, fullCellText().length() - 2);
        // splits query into an ArrayList of tokens
        List<String> values = new ArrayList<>(Arrays.asList(formula.split(" ")));
        // when the getdoubleValue method is called, the FormulaCell sets itself as being 'used.'
        // This makes it so that when abbreviatedCellText is called on a FormulaCell, and that formula contains itself, it won't get evaluated because it's already in use when it's being evaluated
        setUse(true);
        // calls calculator object's evaluate method that has a copy of the
        // spreadsheet. allows for reference to other cells in spreadsheet!
        // returns the value returned by evaluate method
        String value = calculator.evaluate(values);
        if (value.startsWith("#ERROR")) {
            isError = true;
            return 0;
        }
        isError = false;
        setUse(false);
        return Double.parseDouble(value);
    }

    // overridden to handle errors
    public String abbreviatedCellText() {
        double value = getDoubleValue();
        // if there is an error in evaluation, then just return #ERROR instead of any value
        if (isError) {
            return "#ERROR    ";
        }
        // formulacell has been completely evaluated, so it can be set to not being used
        setUse(false);
        return (value + "         ").substring(0, 10);
    }

    public String getCellType() {
        return "FormulaCell";
    }

}
