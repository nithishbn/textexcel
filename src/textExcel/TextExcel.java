// Nithish Narasimman
// Mr. Apley
// 3/28/2018
// TextExcel Part B Final
// This program is a very rudimentary but cool version of Excel all done in the Java console!
// It supports text, numbers, percents, and even a few formulas like SUM and AVG
// it also supports file saving and loading for "professional" use

package textExcel;

import java.util.Scanner;

// Update this file with your own code.

public class TextExcel
{

	public static void main(String[] args)
	{
		Grid spreadsheet = new Spreadsheet();
		System.out.println(spreadsheet.getGridText());
		Scanner console = new Scanner(System.in);
	    while(true){
	    	System.out.print("Enter command: ");
	    	String command = console.nextLine();
	    	if(command.equals("quit")){
	    		System.out.println("Goodbye!");
	    		break;
	    	}
	    	System.out.println(spreadsheet.processCommand(command));
            spreadsheet.recordCommand(command);
	    }
	}
}
